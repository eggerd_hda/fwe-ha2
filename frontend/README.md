# FWE-SS19 Exercise 2 - Frontend
This is the awesome frontend that was teased in the description of the backend server... so, here it is! It utilizes [Material Design](https://material.io/) to get a modern and clean look, and is fully responsive so that it can be used on mobile and desktop devices alike.


## Code Style
This project is linted using [TSLint Recommended](https://github.com/palantir/tslint/blob/master/src/configs/recommended.ts).


## Features
- Single user system
- Utilizes [Material Design](https://material.io/) for a modern and clean look
- Fully responsive so that it can be used on mobile and desktop devices alike
- Easily add, delete, or edit activities
- View all your future or past activities
- Download all your activities as CSV


## Freestyle Feature / Offline Functionality
The app automatically detects whether the backend server is available or not, and will display an offline indicator until the server is available again. Furthermore, in case your current page failed to load its content, it will automatically resume loading and then display the actual page content (without doing a page refresh).

*Example:*  
> You are on the dashboard as the app suddenly looses connection to the backend server. Even tough the app displays the offline indicator, you decide to try to open the details of an activity anyway. However, this fail with a "*something went wrong*" error message. But, the activity details you wanted to see will automatically show up as soon as the backend server is available again... just wait.

This is realized by periodically pinging the server, to check for its availability. Other components can subscribe to the current offline status of the app, so that they can react if the app goes into offline mode or gets back online again. The logic of this feature is implemented in the `AppService`.


## Screenshots

##### Mobile
| Dashboard | Details | Edit |
| --- | --- | --- |
| ![mobile_dashboard](https://gitlab.com/eggerd_hda/fwe-ha2/-/raw/assets/mobile_dashboard.png) | ![mobile_details](https://gitlab.com/eggerd_hda/fwe-ha2/-/raw/assets/mobile_details.png) | ![mobile_edit](https://gitlab.com/eggerd_hda/fwe-ha2/-/raw/assets/mobile_edit.png) |


##### Desktop
| Dashboard | Details | Edit |
| --- | --- | --- |
| ![desktop_dashboard](https://gitlab.com/eggerd_hda/fwe-ha2/-/raw/assets/desktop_dashboard.png) | ![desktop_details](https://gitlab.com/eggerd_hda/fwe-ha2/-/raw/assets/desktop_details.png) | ![desktop_edit](https://gitlab.com/eggerd_hda/fwe-ha2/-/raw/assets/desktop_edit.png) |


#### Explanations
The following explanations reference the mobile and desktop screenshots above and describe the layout from top to bottom.


##### Dashboard
- In the toolbar you find a labeled button for creating a new activity. Right next to it is an icon button that starts the CSV export. You can press and hold it to get a tooltip describing its function/meaning - like every icon button or descriptive icon of this app
- Below the toolbar are the tabs for *future* and *past* activities. The colored badges one those tabs show the amount of activities for the respective tab
- Next comes the actual list of activities
  - The list is sorted by start date and priority (note that the first two activities are listed under *future* even though they started in 2000, because they haven't ended yet and have not been marked as *done*)
  - The colors that can be seen on the left of some activities indicate the priority (which color means which should be pretty straight forward, but they are also labeled in the details or when creating/editing an activity)
  - On the right you find the start date and time, as well as the category and an indicator for participants, which you can hover/hold to get a tooltip with the names. However, the category and participants indicator are hidden on smaller screens
  - Clicking/tabbing an activity will open up the details view for this activity


##### Details
- The top right of the toolbar holds all possible actions for this activity: Marking as *done* (which moves it to *past*), edit, and delete
- The first card holds the title, priority, description, and category (if provided)
- The second card holds the location as well as the start and end date/time.
- The third card shows the list of participants and is only visible if there are any participants
- The fourth card shows the holiday if the start date falls onto one (backend freestyle feature), as well as timestamps for last updated and created


##### Edit & Add
- The back button in the toolbar will ask for confirmation, if the form contains any unsaved input
- The cards are structured just like those of the detail view
- Depending on the browser, a date picker will be shown when entering a start or end date
- Existing participants can only be removed, using the red bin icon on their right


## Requirements
- The backend server has to be running and listening on port `3000`. Please have a look at `/backend/README.md` for details on setting up and running the backend server
- [Node.js](https://nodejs.org) (tested with v11.10)
- [Angular CLI](https://www.npmjs.com/package/@angular/cli) (v7.x)
- A modern browser (i.e. **not** Internet Explorer)


## Installation

##### Docker
The easiest way is to use [Docker](https://www.docker.com/). The provided image already contains all dependencies, configured and ready to work - including some example data (like seen in the [screenshots](#screenshots)).

Execute the following command to download and start the image. But make sure that port `3000` and `8080` aren't already used on your system. You may change port `8080` to something else, but changing port `3000` will cause the frontend to lose connection to the backend server (so you should not change this one)!
```sh
docker run -it -p 3000:3000 -p 8080:8080 eggerd/hda-fwe-ha
```

Wait a view seconds until a message containing `waiting for connections` appears in the last line of the terminal. After that, you are ready to open `http://localhost:8080` in any browser you like (may god have mercy on you, if you *like* Internet Explorer).


##### Manual (Linux only)
Please have a look at `/backend/README.md` for details on setting up and running the backend server, before starting to setup the frontend. In case you would also like to use the example data, you can [download it as JSON](https://gitlab.com/eggerd_hda/fwe-ha2/-/blob/assets/example-data.json) and import it using [mongoimport](https://docs.mongodb.com/manual/reference/program/mongoimport/) or [MongoDB Compass](https://www.mongodb.com/products/compass).

1. Clone the repository:
    ```sh  
    git clone https://gitlab.com/eggerd_hda/fwe-ha2.git
    cd fwe-ha2/frontend
    ```

1. Install the dependencies:
    ```sh  
    npm install
    ```

1. Build the app:
    ```sh  
    ng build --prod
    ```

1. Start a webserver serving the app:
    ```sh  
    npx http-server --gzip --proxy http://localhost:8080? dist/activityFrontend
    ```

1. Open `http://localhost:8080` in your browser
