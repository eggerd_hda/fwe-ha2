import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ActivityDetailComponent } from './activities/activity-detail/activity-detail.component';
import { ActivityEditComponent } from './activities/activity-edit/activity-edit.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'activity/new',
    component: ActivityEditComponent
  },
  {
    path: 'activity/:id',
    component: ActivityDetailComponent
  },
  {
    path: 'activity/:id/edit',
    component: ActivityEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
