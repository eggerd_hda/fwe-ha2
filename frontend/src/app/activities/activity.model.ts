export class Activity {
  id: string;
  name: string;
  description: string;
  location: string;
  priority: number;
  startDate: Date;
  endDate: Date;
  category: string;
  participants: string[];
  holiday: string;
  done: boolean;
  createdAt: Date;
  updatedAt: Date;

  constructor(data?) {
    if (data) {
      this.id = data._id;
      this.name = data.name;
      this.description = data.description;
      this.location = data.location;
      this.priority = data.priority;
      this.startDate = new Date(data.startDate);
      this.endDate = new Date(data.endDate);
      this.category = data.category;
      this.participants = data.participants;
      this.holiday = data.holiday;
      this.done = data.done;
      this.createdAt = new Date(data.createdAt);
      this.updatedAt = new Date(data.updatedAt);
    }
  }

  /**
   * Returns the numerical priority:
   * 0 = None
   * 1 = Low
   * 2 = Medium
   * 3 = High
   */
  getPriority(): number {
    if (this.priority > 3 || this.priority < 0) { return 0; }
    return this.priority;
  }

  /**
   * Returns the priority as a expressed string ("No/Low/Medium/High Priority")
   */
  getExpressedPriority(): string {
    let expressed: string;
    switch (this.getPriority()) {
      case 1: expressed = 'Low'; break;
      case 2: expressed = 'Medium'; break;
      case 3: expressed = 'High'; break;
      default: expressed = 'No'; break;
    }

    return `${expressed} Priority`;
  }
}
