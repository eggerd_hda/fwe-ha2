import { Component, Input } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Activity } from '../activity.model';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})

export class ActivityComponent {
  /** The activity that will be displayed */
  @Input() activity: Activity;

  constructor(private appService: AppService) { }

  /**
   * Returns the name of all participants as a comma separated list
   */
  getParticipants(): string {
    const list = this.activity.participants;
    if (!list) { return; }
    if (list.length === 2) { return list.join(' & '); }
    return list.join(', ');
  }

  /**
   * Returns the start date and time as a formatted human readable string, using the global
   * configuration
   */
  getFormattedStartDate(): string {
    return this.appService.getFormattedDateTime(this.activity.startDate);
  }
}
