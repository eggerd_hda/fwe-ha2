import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { Activity } from '../activity.model';
import { ActivityService } from '../activity.service';

@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.scss']
})

export class ActivityListComponent implements OnInit {
  /** List of all future activities */
  activities: Activity[] = [];

  /** List of all past activities */
  pastActivities: Activity[] = [];

  /** Various status flags */
  status = {
    loading: { // activities are still loading - displaying the loading animation
      future: true,
      past: true,
    },
    error: { // some error occurred while talking to the API
      future: false,
      past: false
    }
  };

  constructor(private activityService: ActivityService, private appService: AppService) { }

  ngOnInit() {
    this.fetchFutureActivities();
    this.fetchPastActivities();

    // observe offline status and retry fetching activities when back online
    this.appService.isOffline.subscribe((offline: boolean) => {
      if (!offline && !this.activities.length) {
        this.fetchFutureActivities();
      }

      if (!offline && !this.pastActivities.length) {
        this.fetchPastActivities();
      }
    });
  }

  /**
   * Loads all future activities
   */
  fetchFutureActivities(): void {
    this.status.loading.future = true;
    this.status.error.future = false;

    this.activityService.getActivities().subscribe((activities) => {
      this.activities = activities;
      this.status.loading.future = false;
    },
    (error) => {
      console.error(error);
      this.status.loading.future = false;
      this.status.error.future = true;
    });
  }

  /**
   * Loads all past activities
   */
  fetchPastActivities(): void {
    this.status.loading.past = true;
    this.status.error.past = false;

    this.activityService.getActivities(true).subscribe((activities) => {
      this.pastActivities = activities;
      this.status.loading.past = false;
    },
    (error) => {
      console.error(error);
      this.status.loading.past = false;
      this.status.error.past = true;
    });
  }
}
