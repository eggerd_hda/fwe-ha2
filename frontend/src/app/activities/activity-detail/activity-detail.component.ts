import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { Activity } from '../activity.model';
import { ActivityService } from '../activity.service';

@Component({
  selector: 'app-activity-detail',
  templateUrl: './activity-detail.component.html',
  styleUrls: ['./activity-detail.component.scss']
})

export class ActivityDetailComponent implements OnInit, OnDestroy {
  private sub: any;

  /** The ID that was specified in the URL */
  id: string;

  /** The activity that will be displayed */
  activity: Activity;

  /** Various status flags */
  status = {
    loading: true, // activity is still loading - displaying the loading animation
    pending: false, // an action is still pending and no other actions should be allowed
    notFound: false, // the requested activity could not be found
    error: false // some error occurred while talking to the API
  };

  constructor(
    private route: ActivatedRoute,
    private activityService: ActivityService,
    private appService: AppService,
    private router: Router
  ) { }

  ngOnInit() {
    // get the activity ID that was specified in the URL
    this.sub = this.route.params.subscribe(params => this.id = params.id);
    this.fetchActivity();

    // observe offline status and retry fetching the activity when back online
    this.appService.isOffline.subscribe((offline: boolean) => {
      if (!offline && !this.activity && !this.status.notFound) {
        this.fetchActivity();
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  /**
   * Returns the passed date object as a formatted human readable string, using the global
   * configuration
   *
   * @param date the date object that should be turned into a human readable string
   */
  getFormattedDateTime(date: Date): string {
    return this.appService.getFormattedDateTime(date);
  }

  /**
   * Loads all data of the activity
   */
  fetchActivity(): void {
    this.status.loading = true;
    this.status.error = false;

    this.activityService.getActivity(this.id).subscribe((activity) => {
      this.activity = activity;
      this.status.loading = false;
    },
    (error) => {
      console.error(error);
      this.status.loading = false;

      if (error.status === 404) {
        this.status.notFound = true;
      } else {
        this.status.error = true;
      }
    });
  }

  /**
   * Permanently deletes the current activity, if confirmed by the user
   */
  deleteActivity(): void {
    if (!confirm('Do you really want to delete this activity permanently?')) { return; }

    this.status.pending = true;
    this.activityService.deleteActivity(this.activity).subscribe(() => {
      this.goBack();
    },
    (error) => {
      console.error(error);
      this.status.pending = false;
      this.appService.showGenericErrorSnackBar();
    });
  }

  /**
   * Mark the current activity as done or undone, depending on its current state. Marking an
   * activity as done will ask the user for confirmation
   */
  toggleDone(): void {
    // only ask for confirmation if marking as "done"
    if (!this.activity.done) {
      if (!confirm('Do you really want to mark this activity as done?')) { return; }
    }

    this.status.pending = true;
    this.activity.done = !this.activity.done;
    this.activityService.updateActivity(this.activity).subscribe(() => {
      this.goBack();
    },
    (error) => {
      console.error(error);
      this.status.pending = false;
      this.activity.done = !this.activity.done;
      this.appService.showGenericErrorSnackBar();
    });
  }

  /**
   * Navigates back to the dashboard
   */
  goBack(): void {
    this.router.navigate(['dashboard']);
  }
}
