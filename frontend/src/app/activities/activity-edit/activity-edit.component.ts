import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
import { Activity } from '../activity.model';
import { ActivityService } from '../activity.service';

@Component({
  selector: 'app-activity-edit',
  templateUrl: './activity-edit.component.html',
  styleUrls: ['./activity-edit.component.scss']
})

export class ActivityEditComponent implements OnInit, OnDestroy {
  private sub: any;

  /** The ID that was specified in the URL */
  id: string;

  /** The activity that will be displayed */
  activity: Activity;

  /** Temporary storage for form input, that can't be bound directly to the activity object */
  input = {
    priority: '0', // the priority (must be a string)
    startDate: '', // start date as YYYY-MM-DD
    startTime: '', // start time as HH-MM-SS (or empty, because not required)
    endDate: '', // end date as YYYY-MM-DD
    endTime: '', // end time as HH-MM-SS (or empty, because not required)
    newParticipant: '' // name of the new participant being added
  };

  /** Various status flags */
  status = {
    loading: true, // activity is still loading - displaying the loading animation
    pending: false, // an action is still pending and no other actions should be allowed
    valid: true, // form is valid an can be submitted
    dateError: false, // endDate is earlier than startDate
    notFound: false, // the requested activity could not be found
    error: false // some error occurred while talking to the API
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private activityService: ActivityService,
    private appService: AppService
  ) { }

  ngOnInit() {
    // get the activity ID that was specified in the URL
    this.sub = this.route.params.subscribe(params => this.id = params.id);
    if (this.id) {
      this.fetchActivity();
    } else {
      // if no ID has been specified, the view will be in "ADD" mode
      this.activity = new Activity();
      this.status.loading = false;
    }

    // observe offline status and retry fetching the activity when back online
    this.appService.isOffline.subscribe((offline: boolean) => {
      if (!offline && this.id && !this.activity && !this.status.notFound) {
        this.fetchActivity();
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  /**
   * Returns the date of the given date object as YYYY-MM-DD
   */
  getFormDate(date: Date): string {
    return date.toISOString().split('T')[0];
  }

  /**
   * Returns the time of the given date object as HH:MM:SS
   */
  getFormTime(date: Date): string {
    return date.toTimeString().split(' ')[0];
  }

  /**
   * Initializes the form with the current data of the activity
   */
  initFormInput(): void {
    this.input.priority = this.activity.getPriority().toString();
    this.input.startDate = this.getFormDate(this.activity.startDate);
    this.input.startTime = this.getFormTime(this.activity.startDate);
    this.input.endDate = this.getFormDate(this.activity.endDate);
    this.input.endTime = this.getFormTime(this.activity.endDate);
  }

  /**
   * Apply the values of the temporary input storage, converting them into the correct type/format
   */
  applyForm(): void {
    this.activity.priority = Number(this.input.priority);
    this.activity.startDate =
      new Date(`${this.input.startDate}T${this.input.startTime ? this.input.startTime : '00:00'}`);
    this.activity.endDate =
      new Date(`${this.input.endDate}T${this.input.endTime ? this.input.endTime : '00:00'}`);
  }

  /**
   * Adds a new participant to the list
   */
  addParticipant(): void {
    if (!this.activity.participants) { this.activity.participants = []; }
    this.activity.participants.push(this.input.newParticipant);
    this.input.newParticipant = '';
  }

  /**
   * Removes the participant at the given index from the list
   *
   * @param index The array index of the participant that should be removed
   */
  removeParticipant(index: number) {
    this.activity.participants.splice(index, 1);
  }

  /**
   * Validates the form - currently only the date input, as everything else can be validated using
   * native HTML
   */
  validate(form: NgForm): void {
    let errors = 0;
    const startDate =
      new Date(`${this.input.startDate}T${this.input.startTime ? this.input.startTime : '00:00'}`);
    const endDate =
      new Date(`${this.input.endDate}T${this.input.endTime ? this.input.endTime : '00:00'}`);

    // if a start & end-date have been specified, validate that startDate <= endDate
    if (!isNaN(startDate.getTime()) && !isNaN(endDate.getTime())) {
      if (startDate <= endDate) { this.status.dateError = false; } else {
        this.status.valid = false; this.status.dateError = true; errors++;
      }
    }

    // if manual validation didn't fail, apply native validation status
    if (!errors) { this.status.valid = form.valid; }
  }

  /**
   * Loads all data of the activity
   */
  fetchActivity(): void {
    this.status.loading = true;
    this.status.error = false;

    this.activityService.getActivity(this.id).subscribe((activity) => {
      this.activity = activity;
      this.initFormInput();
      this.status.loading = false;
    },
    (error) => {
      console.error(error);
      this.status.loading = false;

      if (error.status === 404) {
        this.status.notFound = true;
      } else {
        this.status.error = true;
      }
    });
  }

  /**
   * Save all changes to the current activity, or create a new one if no ID was specified
   */
  saveActivity(): void {
    this.applyForm();
    this.activity.updatedAt = new Date();
    this.status.pending = true;

    if (this.id) {
      this.activityService.updateActivity(this.activity).subscribe(() => {
        this.goBack();
      },
      (error) => {
        console.error(error);
        this.status.pending = false;
        this.appService.showGenericErrorSnackBar();
      });
    } else {
      this.activityService.addActivity(this.activity).subscribe((activity) => {
        this.id = activity.id;
        this.goBack();
      },
      (error) => {
        console.error(error);
        this.status.pending = false;
        this.appService.showGenericErrorSnackBar();
      });
    }
  }

  /**
   * Navigates back to the details page of the activity, or back to the dashboard if no activity
   * was edited/added
   */
  goBack(): void {
    if (this.id) {
      this.router.navigate(['activity', this.id]); // back to details
    } else {
      this.router.navigate(['dashboard']); // back to dashboard
    }
  }
}
