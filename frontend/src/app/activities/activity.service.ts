import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators';
import { Activity } from './activity.model';

@Injectable({
  providedIn: 'root'
})

export class ActivityService {
  /** URL to the activity API (without trailing slash) */
  private apiUrl = 'http://localhost:3000/api/activities';

  constructor(private http: HttpClient) {}

  /**
   * Load all future or past activities. Future activities will be loaded by default
   *
   * @param past Whether past activities should be loaded, instead of future ones
   */
  getActivities(past = false): Observable<Activity[]> {
    return this.http.get<Activity[]>(`${this.apiUrl}${past ? '?past' : ''}`).pipe(
      map(response => {
        return response['data'].map(data => new Activity(data));
      })
    );
  }

  /**
   * Load a single activity by ID
   *
   * @param id The activity ID
   */
  getActivity(id: string): Observable<Activity> {
    return this.http.get<Activity>(`${this.apiUrl}/${id}`).pipe(
      map(response => {
        return new Activity(response['data']);
      })
    );
  }

  /**
   * Create a new activity. Returns a new activity object that will be created using the servers
   * response - including the assigned ID
   *
   * @param activity The activity that should be created
   */
  addActivity(activity: Activity): Observable<Activity> {
    return this.http
      .post(this.apiUrl, activity)
      .pipe(map(response => new Activity(response['data'][0])));
  }

  /**
   * Update an activity. Returns a new activity object that will be created using the servers
   * response - reflecting the new state
   *
   * @param activity The activity that should be updated
   */
  updateActivity(activity: Activity): Observable<Activity> {
    return this.http
      .put(`${this.apiUrl}/${activity.id}`, activity)
      .pipe(map(response => new Activity(response['data'])));
  }

  /**
   * Delete an activity
   *
   * @param activity The activity that should be deleted
   */
  deleteActivity(activity: Activity): Observable<object> {
    return this.http.delete(`${this.apiUrl}/${activity.id}`);
  }

  /**
   * Download all activities in CSV format; returned as plain text
   */
  downloadAllActivities(): Observable<string> {
    return this.http.get(`${this.apiUrl}/csv`, {responseType: 'text'});
  }
}
