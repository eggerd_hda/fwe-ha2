import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-placeholder',
  templateUrl: './placeholder.component.html',
  styleUrls: ['./placeholder.component.scss']
})

export class PlaceholderComponent {
  /** Whether the displayed icon should be rotated periodically (intended for loading animations) */
  @Input() rotating = true;

  /** The name of the material icon that should be displayed */
  @Input() icon = 'hourglass_empty';

  constructor() { }
}
