import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AppService {
  private offlineSnackBar: MatSnackBarRef<SimpleSnackBar>;
  isOffline = new Subject();

  constructor(private snackBar: MatSnackBar, private http: HttpClient) {
    this.pingServer('http://localhost:3000/ping');
  }

  /**
   * Returns the given date in human readable format (DD.MM.YYYY, HH:MM)
   */
  getFormattedDateTime(date: Date): string {
    return date.toLocaleDateString('de-DE', {
      day: '2-digit',
      month: '2-digit',
      year: 'numeric',
      hour: '2-digit',
      minute: '2-digit'
    });
  }

  /**
   * Shows a generic error for failed actions in the snack bar
   */
  showGenericErrorSnackBar() {
    this.snackBar
      .open('Sorry, something went wrong while talking to the server. Please try again later.', 'OK');
  }

  /**
   * Shows or hides the offline message
   *
   * @param show Whether the message should be shown or not
   */
  toggleOfflineSnackBar(show: boolean) {
    if (show && !this.offlineSnackBar) {
      this.offlineSnackBar = this.snackBar.open(
        'Offline – No connection to the server! 💔', null, {panelClass: 'center'});
      this.offlineSnackBar.afterDismissed().subscribe(() => {
        this.offlineSnackBar = null;
      });
    }

    if (!show && this.offlineSnackBar) {
      this.snackBar.open('Back online!', null, {duration: 2000, panelClass: 'center'});
    }
  }

  /**
   * Regularly pings the server to check its availability. In case the server can't be reached, an
   * offline message will be shown
   *
   * @param address Address of the server to ping
   */
  pingServer(address: string): void {
    setTimeout(() => {
      this.http.get(address, {responseType: 'text'}).subscribe(() => {
        // notify subscribers, if the app is currently in offline mode
        if (this.offlineSnackBar) {
          this.isOffline.next(false);
        }

        this.toggleOfflineSnackBar(false);
        this.pingServer(address);
      },
      (error) => {
        // notify subscribers, if the app is not already in offline mode
        if (!this.offlineSnackBar) {
          this.isOffline.next(true);
        }

        this.toggleOfflineSnackBar(true);
        this.pingServer(address);
      });
    }, 5000);
  }
}
