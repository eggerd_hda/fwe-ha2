import { Component } from '@angular/core';
import fileDownload from 'js-file-download';
import { ActivityService } from '../activities/activity.service';
import { AppService } from '../app.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent {
  /** Various status flags */
  status = {
    downloading: false // download is still in progress
  };

  constructor(
    private activityService: ActivityService,
    private appService: AppService
  ) { }

  /**
   * Downloads all activities as CSV file
   */
  downloadAll() {
    this.status.downloading = true;
    this.activityService.downloadAllActivities().subscribe(response => {
      fileDownload(response, 'activities.csv');
      this.status.downloading = false;
    },
    (error) => {
      console.error(error);
      this.status.downloading = false;
      this.appService.showGenericErrorSnackBar();
    });
  }
}
