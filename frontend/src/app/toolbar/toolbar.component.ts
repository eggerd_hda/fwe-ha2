import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})

export class ToolbarComponent {
  /** Title of the toolbar */
  @Input() caption: string;

  /** Whether the user has to confirm before navigating to the previous location */
  @Input() confirmBack = false;

  /** Emits an event if the back button is pressed (which is only visible if there are subscribers) */
  @Output() back: EventEmitter<any> = new EventEmitter();

  constructor() { }

  /**
   * Emits the "back" event. If enabled, the user will be asked to confirm this action,
   * warning him that all unsaved changes will be discarded
   */
  goBack() {
    if (this.confirmBack) {
      if (!confirm('Do you really want to discard your changes?')) { return; }
    }

    this.back.emit();
  }
}
