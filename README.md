# Fortgeschrittene Webentwicklung - Hausaufgabe
*5\. Semester, 2019 (Wahlpflicht)*  
*[Incloud GmbH](https://www.incloud.de): David Müller, M.Sc. / Daniel Schulz, B.Sc. / Tim Kolberger, B.Sc.*  
*[Bevelop GmbH](https://bevelop.de): Daniel Wohlfarth, B.Sc. / Thomas Sauer, B.Sc.*

Um am späteren Gruppenprojekt der Veranstaltung teilnehmen zu können, war zunächst die erfolgreiche Bearbeitung von zwei benoteten Hausaufgaben notwendig. Thema der Hausaufgaben war die Entwicklung eines simplen Aktivitätsplaners - zunächst das Backend und anschließend das Frontend.


## Behandelte Themen
- Aufsetzen eines Web-Services mit [Express.js](https://expressjs.com)
- Angular Grundlagen (Komponenten, Services, Routing, etc.)
- [REST Best Practices](https://hackernoon.com/restful-api-designing-guidelines-the-best-practices-60e1d954e7c9)
- Deploy einer Anwendung via [Docker](https://www.docker.com)
- Testing via [Mocha](https://mochajs.org/) & [Chai](https://www.chaijs.com/)
- GitLab CI/CD
- Fehlerbehandlung
- Websecurity


## Anforderungen
- Das System ist "Single User", muss also keine Accounts, Sessions, Rechte, Logins oder Ähnliches haben
- Eine ausführliche Dokumentation des Projekts in der ReadMe-Datei


#### Backend
- Bearbeitungszeit: 28 Tage
- Für das Speichern von Daten kann MongoDB, MySQL, Redis oder JSON-Dateien verwendet werden
- Es darf ein beliebiger Webserver und beliebig viele weitere Libraries verwendet werden
- Eine Aktivität muss mindestens einen Namen, Zeitraum, Ort und eine Beschreibung besitzen. Zudem soll eine Aktivität als erledigt markiert werden können. Weitere Attribute können nach Belieben hinzugefügt werden, solange diese zur App passen und eine sinnvolle Funktion besitzen
- Ein Endpunkt zum Anlegen, Auflisten, Bearbeiten und Löschen von Aktivitäten (CRUD)
- Ein Endpunkt zum Auflisten von ausschließlich zukünftigen oder vergangenen Aktivitäten, basierend auf einem frei definierbarem Datum
- Ein Endpunkt zum Exportieren aller Aktivitäten als CSV
- Tests die alle Fälle abdecken, bereitgestellt als Postman Collection oder als Script (z.B. via Mocha & Chai)
- Implementiere ein nicht gefordertes Feature das zur App passt, um Bonuspunkte zu erhalten. Sollte dabei eine externe API verwendet werden, gibt dies zusätzlich Bonuspunkte


#### Frontend
- Bearbeitungszeit: 30 Tage
- Es darf ausschließlich Angular 7 verwendet werden
- Es dürfen beliebige weitere Frameworks (z.B. für Styling) verwendet werden
- Eine Listen- und Detailansicht für Aktivitäten
- Aus den jeweiligen Ansichten sollten sich die Daten anlegen, ändern und ggf. löschen lassen (CRUD)
- Eine Übersicht, in der die Anzahl der vergangenen und zukünftigen Aktivitäten ersichtlich ist
- Download aller Aktivitäten als CSV-Datei
- Es sollen mindestens zwei eigene Komponenten, ein Router, eine Direktive und ein Service verwendet werden
- Eine Funktionalität, die auch ohne den Backend Server lauffähig ist
- Implementiere ein nicht gefordertes Feature das zur App passt, um Bonuspunkte zu erhalten


## Endergebnis

#### Backend
- Die originale [Dokumentation des Backends](https://gitlab.com/eggerd_hda/fwe-ha2/-/tree/master/backend#readme) ist in der `/backend/README.md` zu finden und umfasst implementierte Features, Anforderungen, Installationsanweisungen, Konfigurationsoptionen, Tests und natürlich die API (inkl. Beispielen)
- Aktivitäten wurden um eine Priorität, Kategorie und eine Liste von Teilnehmern erweitert
- Zum Speichern der Daten wird MongoDB verwendet
- Als zusätzliches Feature wurde eine automatische Markierung von Feiertagen implementiert. Hierzu wird beim Erstellen oder Bearbeiten einer Aktivität überprüft, ob das Startdatum auf einen deutschen Feiertag fällt. Verwendet wird hierfür die API von [Calenderific](https://calendarific.com/)
- Die Tests wurden mithilfe von [Mocha](https://mochajs.org/) und [Chai](https://www.chaijs.com/) realisiert. Sind zusätzlich aber auch als [Postman Collection](https://www.getpostman.com/collections/ddf4f29e320d507ce1d8) verfügbar
- Der Code ist nach dem [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript) formatiert


#### Frontend
**Mobile Screenshots**

| Dashboard | Details | Edit |
| --- | --- | --- |
| ![mobile_dashboard](https://gitlab.com/eggerd_hda/fwe-ha2/-/raw/assets/mobile_dashboard.png) | ![mobile_details](https://gitlab.com/eggerd_hda/fwe-ha2/-/raw/assets/mobile_details.png) | ![mobile_edit](https://gitlab.com/eggerd_hda/fwe-ha2/-/raw/assets/mobile_edit.png) |


**Desktop Screenshots**

| Dashboard | Details | Edit |
| --- | --- | --- |
| ![desktop_dashboard](https://gitlab.com/eggerd_hda/fwe-ha2/-/raw/assets/desktop_dashboard.png) | ![desktop_details](https://gitlab.com/eggerd_hda/fwe-ha2/-/raw/assets/desktop_details.png) | ![desktop_edit](https://gitlab.com/eggerd_hda/fwe-ha2/-/raw/assets/desktop_edit.png) |

- Die originale [Dokumentation des Frontends](https://gitlab.com/eggerd_hda/fwe-ha2/-/tree/master/frontend#readme) ist in der `/frontend/README.md` zu finden und umfasst implementierte Features, Anforderungen, Installationsanweisungen und Beschreibungen der einzelnen Elemente anhand der Screenshots
- Verwendet [Material Design](https://material.io/) für einen modernen und sauberen Look
- Vollständig responsive, sodass es auf Mobilgeräten und Desktops verwendet werden kann
- Als zusätzliches Feature wurde eine Offline-Erkennung implementiert. Die App zeigt einen Indikator an, falls der Backend-Server nicht erreichbar ist. Falls zudem das Laden von Daten fehlschlug, werden diese automatisch neu geladen, sobald der Backend-Server wieder erreichbar ist
- Es wurde ein Docker Image erstellt, welches sowohl das Frontend als auch das Backend inkl. Beispieldaten bereitstellt. Zudem wurde eine Pipeline konfiguriert, die das Docker Image bei Änderungen am Code automatisch aktualisiert
- Der Code ist nach dem [TSLint Recommended](https://github.com/airbnb/javascript) Style formatiert


## Demo

#### Installation

##### Docker
Der einfachste Weg ist die Verwendung von [Docker](https://www.docker.com/), da das bereitgestellte [Image](https://hub.docker.com/r/eggerd/hda-fwe-ha) sowohl den Backend-Server als auch das Frontend bereitstellt und bereits einige Beispieldaten (wie in den Screenshots) enthält.

Über den nachfolgenden Befehl kann das Image heruntergeladen und gestartet werden. Es ist zu beachten, dass der Port `3000` nicht geändert werden darf, da das Frontend sonst keine Verbindung zum Backend-Server aufbauen kann. 
```sh
docker run -it -p 3000:3000 -p 8080:8080 eggerd/hda-fwe-ha
```
Nach Ausführen des Befehls einfach einige Sekunden warten, bis in der Konsole kein neuer Text mehr erscheint. Anschließen kann das Frontend unter `http://localhost:8080` in einem beliebigen *modernen* Browser (also *nicht* Internet Explorer) aufgerufen werden.


##### Manuell (nur Linux)
Die manuelle Installation ist in der originalen [ReadMe des Backends](https://gitlab.com/eggerd_hda/fwe-ha2/-/tree/master/backend#readme) und in der [ReadMe des Frontends](https://gitlab.com/eggerd_hda/fwe-ha2/-/tree/master/frontend#readme) ausführlich beschrieben - die jeweiligen Requirements sind zu beachten.


#### REST-Schnittstelle
Eine ausführliche Dokumentation der REST-Schnittstelle ist in der [ReadMe des Backend-Servers](https://gitlab.com/eggerd_hda/fwe-ha2/-/tree/master/backend#readme) zu finden. Für die Kommunikation mit der REST-Schnittstelle wird die Verwendung von [Postman](https://www.getpostman.com/) empfohlen. Die [bereitgestellte Collection](https://www.getpostman.com/collections/ddf4f29e320d507ce1d8) enthält verschiedenste Beispiele für jeden Endpunkt.

Alternativ eignet sich auch Firefox für die Kommunikation mit der REST-Schnittstelle, da dieser die in JSON formatierten Antworten des Server automatisch in einem für Menschen besser lesbaren Format anzeigt; zum Senden von POST-Requests kann die Erweiterung "[RESTED](https://addons.mozilla.org/de/firefox/addon/rested/)" verwendet werden.


## Beteiligte Personen
- [Dustin Eckhardt](https://gitlab.com/eggerd)


## Verwendete Software
- [Node.js](https://nodejs.org/) (11.10)
- [TypeScript](https://www.typescriptlang.org/) (3.4.1)
- [TSLint](https://palantir.github.io/tslint/) (5.16.0)


##### Backend
- [Express](https://expressjs.com) (4.16.4)
- [MongoDB](https://www.mongodb.com/) (4.0)
- [Mongoose](https://mongoosejs.com/) (5.5.2)
- [Request](https://github.com/request/request) (2.88.0)
- [Mocha](https://mochajs.org/) (6.1.4)
- [Chai](https://www.chaijs.com/) (4.2.0)


##### Frontend
- [Angular](https://angular.io/) (7.2.0)
- [Angular Material](https://material.angular.io/) (7.3.7)
- [Hammer.JS](http://hammerjs.github.io/) (2.0.8)
- [RxJS](https://github.com/ReactiveX/RxJS) (6.3.3)
- [Docker](https://www.docker.com/)
