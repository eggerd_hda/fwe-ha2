import { ObjectID } from 'bson';
import config from 'config';
import { NextFunction, Request, Response } from 'express';
import { Parser } from 'json2csv';
import { InvalidDateError, InvalidIdError, ValidationError } from '../custom/errors';
import { activityModel, IActivityModel } from '../model/activity';
import requestPromise = require('request-promise');

/**
 * Get all existing activities. By default, only future activities will be returned, based on the
 * current date and time. However, the date and time can be overwritten, using the "date" parameter.
 * You may also use a "past" parameter flag to indicate that only past activities should be returned
 * instead.
 *
 * @see getFutureActivities
 * @see getPastActivities
 *
 * @param {Request} request the request object
 * @param {Response} response the response object
 * @param {NextFunction} next the next function in the chain
 */
export const getActivities = async (request: Request, response: Response, next: NextFunction) => {
  // get past activities, if a "past" query parameter is present
  if (request.query.past !== undefined) {
    getPastActivities(request, response, next);
  } else {
    getFutureActivities(request, response, next);
  }
};

/**
 * Get all past activities, based on a date and time. If no date and time is provided via "date"
 * parameter, the current date and time will be used instead.
 *
 * @param {Request} request the request object
 * @param {Response} response the response object
 * @param {NextFunction} next the next function in the chain
 */
export const getPastActivities =
  async (request: Request, response: Response, next: NextFunction) => {
    // validate syntax of specified date, or use current time if none was specified
    const date = request.query.date ? new Date(request.query.date) : new Date();
    if (isNaN(date.getTime())) { next(new InvalidDateError()); return; }

    // get all activities whose startDate & endDate are earlier than the specified date
    const pastActivities: IActivityModel[] = await activityModel.find({
      $or: [
        { done: { $eq: true } },
        { endDate: { $lt: date } },
      ],
    }).sort({ startDate: 1, priority: -1 });

    response.send({
      date,
      data: pastActivities,
    });
  };

/**
 * Get all future activities, based on a date and time. If no date and time is provided via "date"
 * parameter, the current date and time will be used instead.
 *
 * @param {Request} request the request object
 * @param {Response} response the response object
 * @param {NextFunction} next the next function in the chain
 */
export const getFutureActivities =
  async (request: Request, response: Response, next: NextFunction) => {
    // validate syntax of specified date, or use current time if none was specified
    const date = request.query.date ? new Date(request.query.date) : new Date();
    if (isNaN(date.getTime())) { next(new InvalidDateError()); return; }

    // get all activities whose startDate or endDate are after the specified date
    const futureActivities: IActivityModel[] = await activityModel.find({
      $or: [
        { startDate: { $gte: date } },
        { endDate: { $gt: date } },
      ],
      $and: [
        { done: { $eq: false } },
      ],
    }).sort({ startDate: 1, priority: -1 });

    response.send({
      date,
      data: futureActivities,
    });
  };

/**
 * Get a single activity by ID
 *
 * @param {Request} request the request object
 * @param {Response} response the response object
 * @param {NextFunction} next the next function in the chain
 */
export const getSingleActivity =
  async (request: Request, response: Response, next: NextFunction) => {
    // check whether the specified ID is valid
    if (!ObjectID.isValid(request.params.activityId)) { next(new InvalidIdError()); return; }

    const singleActivity: IActivityModel | null = await activityModel.findById(
      request.params.activityId,
    );

    if (singleActivity) {
      response.send({
        data: singleActivity,
      });
    }
    else { response.status(404).send(); } // couldn't find an activity for the specified ID
  };

/**
 * Create a new activity
 *
 * @param {Request} request the request object
 * @param {Response} response the response object
 * @param {NextFunction} next the next function in the chain
 */
export const createActivity = async (request: Request, response: Response, next: NextFunction) => {
  // change into array, if not already submitted as one
  request.body = request.body.length ? request.body : [request.body];

  for (const activity of request.body) {
    /**
     * validate that the endDate isn't earlier than the startDate. Incorrect syntaxes of the
     * specified dates are ignored at this point, as this will be caught by the model anyway
     */
    const startDate = new Date(activity.startDate).getTime();
    const endDate = new Date(activity.endDate).getTime();
    if (!isNaN(startDate + endDate) && endDate < startDate) {
      next(new ValidationError("The 'endDate' can not be earlier than the 'startDate'")); return;
    }

    // load holiday name from external API
    activity.holiday = await getHoliday(new Date(activity.startDate));
  }

  const createdActivity: IActivityModel = await activityModel.create(request.body);
  response.status(201).send({
    data: createdActivity,
  });
};

/**
 * Update a single activity by ID
 *
 * @param {Request} request the request object
 * @param {Response} response the response object
 * @param {NextFunction} next the next function in the chain
 */
export const updateActivity = async (request: Request, response: Response, next: NextFunction) => {
  // check whether the specified ID is valid
  if (!ObjectID.isValid(request.params.activityId)) { next(new InvalidIdError()); return; }

  /**
   * validate that the endDate isn't earlier than the startDate. Incorrect syntaxes of the specified
   * dates are ignored at this point, as this will be caught by the model anyway
   */
  const startDate = new Date(request.body.startDate).getTime();
  const endDate = new Date(request.body.endDate).getTime();
  if (!isNaN(startDate + endDate) && endDate < startDate) {
    next(new ValidationError("The 'endDate' can not be earlier than the 'startDate'")); return;
  }

  // load holiday name from external API
  request.body.holiday = await getHoliday(new Date(request.body.startDate));

  const updatedActivity: IActivityModel | null = await activityModel.findByIdAndUpdate(
    request.params.activityId,
    request.body,
    {
      new: true, // return the modified activity, rather than the original one
      runValidators: true,
    },
  );

  if (updatedActivity) {
    response.send({
      data: updatedActivity,
    });
  }
  else { response.status(404).send(); } // couldn't find an activity for the specified ID
};

/**
 * Delete a single activity by ID
 *
 * @param {Request} request the request object
 * @param {Response} response the response object
 * @param {NextFunction} next the next function in the chain
 */
export const deleteActivity = async (request: Request, response: Response, next: NextFunction) => {
  // check whether the specified ID is valid
  if (!ObjectID.isValid(request.params.activityId)) { next(new InvalidIdError()); return; }

  const deletedActivity: IActivityModel | null = await activityModel.findByIdAndDelete(
    request.params.activityId,
  );

  if (deletedActivity) { response.status(204).send(); }
  else { response.status(404).send(); }  // couldn't find an activity for the specified ID
};

/**
 * Exports all activities (past & future) as CSV
 *
 * @param {Request} request the request object
 * @param {Response} response the response object
 */
export const exportAllActivities = async (request: Request, response: Response) => {
  const allActivities: IActivityModel[] = await activityModel.find().lean();
  if (!allActivities.length) { response.status(204).send(); return; } // no activities to export

  const json2csv = new Parser();
  const csv = json2csv.parse(allActivities);
  response.setHeader('Content-disposition', 'attachment; filename=activities.csv');
  response.type('text/csv').send(csv);
};

/**
 * Returns the name of a national holiday for the given date, based on the country configured in
 * "holiday.country". If no holiday can be found, undefined will be returned instead.
 * Needs an API key to be configured in order to work!
 *
 * @param date
 * @returns Promise<string|undefined> test
 */
export const getHoliday = async (date: Date): Promise<string|undefined> => {
  if (!config.get('holidays.apiKey')) { return; }

  const requestOptions: requestPromise.Options = {
    method: 'GET',
    json: true,
    uri: `https://calendarific.com/api/v2/holidays?&api_key=${config.get('holidays.apiKey')}`
      + `&country=${config.get('holidays.country')}&year=${date.getFullYear()}`
      + `&month=${date.getMonth() + 1}&day=${date.getDate()}`,
  };

  const api = await requestPromise(requestOptions);
  if (api.meta.code === 200 && api.response.holidays.length > 0) {
    return api.response.holidays[0].name;
  }
};
