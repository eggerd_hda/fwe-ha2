import { Response, Router, Request } from 'express';
import { activityRouter } from './activity';

export const router: Router = Router({ mergeParams: true });

router.use('/api/activities', activityRouter);

// Used by the frontend to check availability
router.use('/ping', (request: Request, response: Response) => {
  response.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
  response.setHeader('Pragma', 'no-cache');
  response.setHeader('Expires', 0);
  response.send('pong');
});
