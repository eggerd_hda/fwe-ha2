import * as express from 'express';
import * as activityController from '../controller/activity';
import { validationErrorHandler, wrapAsync } from '../middleware/errorhandler';

export const activityRouter: express.Router = express.Router({ mergeParams: true });

/** GET Requests */
activityRouter.get('/', wrapAsync(activityController.getActivities));
activityRouter.get('/csv', wrapAsync(activityController.exportAllActivities));
activityRouter.get('/:activityId', wrapAsync(activityController.getSingleActivity));

/** POST Requests */
activityRouter.post('/', wrapAsync(activityController.createActivity));

/** DELETE Requests */
activityRouter.delete('/:activityId', wrapAsync(activityController.deleteActivity));

/** PUT Requests */
activityRouter.put('/:activityId', wrapAsync(activityController.updateActivity));

/** Middleware */
activityRouter.use(validationErrorHandler);
