import { NextFunction, Request, Response } from 'express';
import { ContentTypeError } from '../custom/errors';

/**
 * Tries to parse the request body. Requires the content-type to be 'application/json', otherwise
 * an error will be raised
 *
 * NOTE: Could be done using bodyParser.json(), but then our sanitizer would need to recursively
 * loop over the body object, instead of just analyzing a simple string
 *
 * @param {Request} request the request object
 * @param {Response} response the response object
 * @param {NextFunction} next the next function in the chain
 */
export const jsonParser = (request: Request, response: Response, next: NextFunction) => {
  if (typeof request.body !== 'string' || request.body.length === 0) { next(); return; }

  // content-type 'application/json' is required
  if (request.headers['content-type'] !== 'application/json') {
    next(new ContentTypeError('application/json', request.headers['content-type']));
    return;
  }

  try {
    request.body = JSON.parse(request.body);
    next();
  } catch (error) {
    next(error);
  }
};
