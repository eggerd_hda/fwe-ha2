import { NextFunction, Request, Response } from 'express';
import { ContentTypeError, ValidationError } from '../custom/errors';

/**
 * Will catch errors of an async function and invoke the next error handler in the chain
 *
 * @param {Function} wrappedAsyncFunc the async function whose errors will be caught
 */
export const wrapAsync = (wrappedAsyncFunc: Function) => {
  return function (request: Request, response: Response, next: NextFunction) {
    wrappedAsyncFunc(request, response, next).catch(next);
  };
};

/**
 * Intended to catch all errors that haven't been caught by any other error handler; will be logged
 * to the console. Will always result in a http status code of 500
 *
 * @param {Error} error the error object
 * @param {Request} request the request object
 * @param {Response} response the response object
 * @param {NextFunction} next the next function in the chain
 */
export const globalErrorHandler =
  (error: Error, request: Request, response: Response, next: NextFunction) => {
    console.error(error);
    response.status(500).send({ error: { name: 'InternalServerError',
      message: 'Something went wrong' } });
  };

/**
 * Handles 'ValidationError' as well as any other error whose name is 'ValidationError' or
 * 'CastError'. All other errors are passed to the next function. Intended to use for all errors
 * that occur while validating user input. Will alway result in a http status code of 400
 *
 * @param {Error} error the error object
 * @param {Request} request the request object
 * @param {Response} response the response object
 * @param {NextFunction} next the next function in the chain
 */
export const validationErrorHandler =
  (error: Error, request: Request, response: Response, next: NextFunction) => {
    if (error.name === 'ValidationError' || error.name === 'CastError' ||
    error instanceof ValidationError) {
      response.status(400).send({ error: { name: error.name, message: error.message } });
    } else {
      next(error);
    }
  };

/**
 * Handles 'SyntaxError' of the JSON parser as well as 'ContentTypeError'. All other errors are
 * passed to the next function. Intended to use for errors of the JSON parser middleware. Syntax
 * errors will result in a http status code of 422, and content-type errors will be 415
 *
 * @param {Error} error the error object
 * @param {Request} request the request object
 * @param {Response} response the response object
 * @param {NextFunction} next the next function in the chain
 */
export const jsonErrorHandler =
  (error: Error, request: Request, response: Response, next: NextFunction) => {
    if (error instanceof SyntaxError) {
      response.status(422).send({ error: { name: error.name, message: error.message } });
    } else if (error instanceof ContentTypeError) {
      response.status(415).send({ error: { name: error.name, message: error.message } });
    } else {
      next(error);
    }
  };
