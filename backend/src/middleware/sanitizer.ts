import { NextFunction, Request, Response } from 'express';
import sanitizeHtml from 'sanitize-html';

/**
 * Sanitizes a requests body and query parameters
 *
 * @param {Request} request the request object
 * @param {Response} response the response object
 * @param {NextFunction} next the next function in the chain
 */
export const sanitizer = (request: Request, response: Response, next: NextFunction) => {
  // overwriting the default settings, which allow certain tags
  const options = { allowedTags: [] };

  // sanitizing the whole request body
  if (typeof request.body === 'string') {
    request.body = sanitizeHtml(request.body, options);
  }

  // also sanitizing the values of ever query parameter
  for (const key in request.query) {
    if (request.query.hasOwnProperty(key)) {
      request.query[key] = sanitizeHtml(request.query[key], options);
    }
  }

  next();
};
