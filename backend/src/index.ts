import * as bodyParser from 'body-parser';
import config from 'config';
import cors from 'cors';
import express from 'express';
import mongoose from 'mongoose';
import { globalErrorHandler, jsonErrorHandler } from './middleware/errorhandler';
import { jsonParser } from './middleware/json-parser';
import { sanitizer } from './middleware/sanitizer';
import { router } from './routes/router';

export const app: express.Application = express();
app.use(cors());

/**
 * Using .text() for all content-types, because this allows the sanitizer to analyse the whole
 * request body as a string, instead of needing to recursively loop over the body object. This is
 * done on app level, instead of in a controller, because HTML isn't allowed in any context of the
 * app anyway
 */
app.use(bodyParser.text({ type: '*/*' }));

// removes all HTML from the request body and its query parameters
app.use(sanitizer);

// tries to parse the request body
app.use(jsonParser);
app.use(jsonErrorHandler);

app.use('/', router);
app.use(globalErrorHandler);

/** Connects to the database (automatically retries if connection fails) */
function connectToDatabase() {
  const dbHost =
    `mongodb://${config.get('database.hostname')}
    :${config.get('database.port')}
    /${config.get('database.name')}`;
  mongoose.connect(dbHost, { useNewUrlParser: true }, (err) => {
    if (err) {
      console.warn('Failed to establish database connection - retrying...', err);
      setTimeout(connectToDatabase, 1000);
    } else {
      console.log('Database connection established');
      mongoose.set('useFindAndModify', false); // to prevent deprecated warnings
    }
  });
}
connectToDatabase();

app.listen(config.get('app.port'), config.get('app.hostname'), () => {
  if (process.env.NODE_ENV !== 'test') {
    console.log(`Server is listening on ${config.get('app.hostname')}:${config.get('app.port')}`);
  }
});
