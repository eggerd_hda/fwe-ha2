import { Document, Model, model, Schema } from 'mongoose';

export interface IActivityModel extends Document {
  name: string;
  description: string;
  location: string;
  startDate: Date;
  endDate: Date;
  done: boolean;
  category: string;
  participants: string[];
  priority: number;
  holiday: string;
}

export const activitySchema: Schema = new Schema(
  {
    name: {
      type: String,
      trim: true,
      required: 'A name for the activity is required',
    },
    description: {
      type: String,
      trim: true,
      required: 'A description of the activity is required',
    },
    location: {
      type: String,
      trim: true,
      required: 'The location of the activity is required',
    },
    startDate: {
      type: Date,
      required: 'The date and time at which the activity starts is required',
    },
    endDate: {
      type: Date,
      required: 'The date and time at which the activity ends is required',
    },
    done: {
      type: Boolean,
      default: false,
    },
    category: {
      type: String,
      trim: true,
      required: false,
    },
    participants: {
      type: [String],
      trim: true,
      default: undefined,
    },
    priority: {
      type: Number,
      default: 0,
    },
    holiday: {
      type: String,
      default: undefined,
    },
  },
  { timestamps: true },
);

export const activityModel: Model<IActivityModel> =
  model<IActivityModel>('Activity', activitySchema);
