/**
 * Error during validation of user input
 */
export class ValidationError extends Error {
  constructor(message: string) {
    super(message);
    this.message = message;
    this.name = 'ValidationError';
    Error.captureStackTrace(this, this.constructor);
  }
}

/**
 * Error while creating a Date object, due to a malformed Date Time String
 */
export class InvalidDateError extends ValidationError {
  constructor() {
    const message = 'Dates have to use the Date Time String Format, e.g. YYYY-MM-DDTHH:mm:ss.sssZ';
    super(message);
    this.message = message;
    this.name = 'InvalidDateError';
    Error.captureStackTrace(this, this.constructor);
  }
}

/**
 * A invalid resource ID was used
 */
export class InvalidIdError extends ValidationError {
  constructor() {
    const message = 'The specified ID is invalid';
    super(message);
    this.message = message;
    this.name = 'InvalidIdError';
    Error.captureStackTrace(this, this.constructor);
  }
}

/**
 * An unexpected content-type was encountered
 */
export class ContentTypeError extends Error {
  /**
   *
   * @param {string} expected the expected content-type, e.g. 'application/json'
   * @param {string} [received] the content-type that was actually received
   */
  constructor(expected: string, received?: string) {
    const message = `Expected content-type '${expected}' but received '${received}'`;
    super(message);
    this.message = message;
    this.name = 'ContentTypeError';
    Error.captureStackTrace(this, this.constructor);
  }
}
