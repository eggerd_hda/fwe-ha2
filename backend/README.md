# FWE-SS19 Exercise 1 - Backend
This is a small server that can manage your activities. Should be used in combination with the awesome frontend... that has yet to be built. In the meantime, you can just use the [RESTful API](#api-reference) or go ahead build your own - surely not so awesome - frontend ;-)


## Code Style
This project is linted using [Airbnb JavaScript Style](https://github.com/airbnb/javascript).


## Features
- Single user system
- [RESTful API](#api-reference)
  - Create, Read, Update, and Delete activities
  - Get past or future activities based on a given date
  - Exporting all activities (past & future) as CSV file
- Activities consist of a Name, Description, Start- & Endtime, Location, Priority, Category, and a list of Participants
- National holidays will automatically be mentioned in your activities


## Freestyle Feature
The server uses the API of [Calendarific](https://calendarific.com/) to check whether the start date of an activity falls on a national holiday. If so, the name of this holiday will be added to the activities details. Those checks are performed whenever an activity is created or updated. By default the checks will look for holidays in Germany, but this can be [configured](#configuration).

In `src/controller/activity.ts` you will find the small helper function `getHoliday()`, that performs the API requests for this feature.


## Requirements
- [Node.js](https://nodejs.org) (tested with v11.10)
- A [MongoDB](https://www.mongodb.com/) server (tested with v4.0)


## Installation
Make sure that you have Node.js installed ([see requirements](#requirements)) and then follow the steps below.

1. Clone the repository:  
    ```sh  
    git clone https://gitlab.com/eggerd_hda/fwe-ha2.git
    cd fwe-ha2/backend
    ```
1. Install the dependencies. If you also want to run tests or the dev environment, remove the `--production` part.
    ```sh
    npm install --production
    ```
1. Build the app:
    ```sh
    npm run build
    ```
1. Adjust the [configuration](#configuration) if needed, or check out [how to use](how-to-use) the server


## Configuration
In `/config` you will find configuration files for production (`default.json`), development (`dev.json`), and tests (`test.json`). By default, all environments are configured to use a MongoDB server ([see requirements](#requirements)) that runs on the same host. Moreover, production is configured to use a database with the name `activities`.

Below you find an overview of all available settings along with a short description, even though they should be pretty obvious.

| Key | Default | Description |
| --- | --- | --- |
| **app** |
| hostname | localhost | The IP on which this server will listen |
| port | 3000 | The port on which this server will listen |
| **database** |
| hostname | localhost | The FQDN of the MongoDB server |
| port | 27017 | The port on which the database server is listening |
| name | activities | The name of the database to use |
| **holidays** |
| country | DE | An [ISO 3166-1 alpha-2](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) country code ([list of supported countries](https://calendarific.com/supported-countries)). Only holidays of the here specified country will be looked up |
| apiKey | *key* | The API key to use for requests to the API of [Calendarific](https://calendarific.com/) |


## Tests

##### Running Tests
You can run all tests by executing the command below.
```sh
npm run test
```

In case you only installed production dependencies earlier, you will need to execute `npm install` again before you can run the tests.

By default, the tests require a MongoDB on `localhost:27012` and port `3030` to be free. So, in case this port is already in use on your system or you wish to use a different database, make sure to adjust the [test configuration](configuration) accordingly, before running the tests.  

Additionally, I created a [Postman Collection](https://www.getpostman.com/collections/ddf4f29e320d507ce1d8) that contains multiple examples for all endpoints.


##### What is being tested?
The app is tested using [Mocha](https://Mochajs.org) and [Chai](https://www.chaijs.com/). The vast majority of tests simply verifies that the [RESTful API](#api-reference) responds with the expected set of data and status code, or that it handles incorrect data correctly and responds with a certain error. In case of `POST`, `PUT`, and `DELETE` requests, it is also verified that the respective action is actually performed on the database.

All tests are located in `/test`, in case you want to check them out. Besides the already easy to read "should"-syntax that they use, they are also commented, which should make it pretty clear what exactly is verified. 

Please note that the [Freestyle Feature](#freestyle-feature) isn't tested with automated tests, because I couldn't figure out how to mock those requests in the remaining time. However, the feature can be tested using the mentioned  [Postman Collection](https://www.getpostman.com/collections/ddf4f29e320d507ce1d8).


## How to use?
```sh
npm run prod
```
Will start the server in production mode. 

```sh
npm run dev
```
Will start the server in for development. The server will automatically restart whenever a source file is changed. In case you only installed production dependencies earlier, you will need to execute `npm install` again before you can start the development environment.

After you started one of the environments, you can start making calls to the [RESTful API](#api-reference) using any browser or tool that is able to make HTTP requests, e.g. [Postman](https://www.getpostman.com/).


## API Reference
The API is available at `/api` and all endpoints mentioned below expect this as their base path. Be aware that the API only accepts content of the type `application/json`.

#### Get All Activities
Get all present and future activities. By default, this is based on the current server time, but a custom date and time can be specified using the `date` URL parameter. Moreover, it can be switched to getting all past activities by using the `past` URL parameter.

|     |     |
| --- | --- |
| **URL** | `/activities?date={date}&past` |
| **Method** | GET |
| **URL Parameters** | `date=[string]` *optional* - The date (in [Date Time String Format](http://www.ecma-international.org/ecma-262/5.1/#sec-15.9.1.15) - e.g. `YYYY-MM-DDTHH:mm:ss.sssZ`) that will be used instead of the current server time |
| | `past` *optional* - Indicates to only get past activities |
| **Data Parameters** | *None* |
| **Success&#160;Response** | 200 OK - [View Content Example](https://codebeautify.org/online-json-editor/cb568efd) |
| **Error Response** | 400 Bad Request - [View Content](https://codebeautify.org/online-json-editor/cb29bcba) (Invalid Date Error) |

```sh
# Example Call - Get all activities past 2020-01-01 00:00:00
curl --request GET --url 'http://example.com:3000/api/activities?past&date=2020'
```


#### Get Single Activity
---
Gets a single activity based on the specified ID.

|     |     |
| --- | --- |
| **URL** | `/activities/{id}` |
| **Method** | GET |
| **URL Parameters** | `id=[string]` *required* - ID of the activity to get |
| **Data Parameters** | *None* |
| **Success&#160;Response** | 200 OK - [View Content Example](https://codebeautify.org/online-json-editor/cb773515) |
| **Error Response** | 400 Bad Request - [View Content](https://codebeautify.org/online-json-editor/cb702754) (Invalid ID Error) |
| | 404 Not Found - *No Content* |

```sh
# Example Call - Get activity with ID 'activities/5cc1ec46a9bf750ea4e1916d'
curl --request GET --url 'http://example.com:3000/api/activities/5cc1ec46a9bf750ea4e1916d'
```


#### Export All Activities
---
Export all activities (past & future) as CSV file.

|     |     |
| --- | --- |
| **URL** | `/activities/csv` |
| **Method** | GET |
| **URL Parameters** | *None* |
| **Data Parameters** | *None* |
| **Success&#160;Response** | 200 OK - [View Content Example](https://codebeautify.org/csv-to-xml-json/cb81b66e) |
| **Error Response** | 204 No Content - *No Content* |

```sh
# Example Call - Exporting all activities as CSV into the file 'activities.csv'
curl --request GET --url 'http://example.com:3000/api/activities/csv' -o activities.csv
```


#### Create New Activity
---
Create a new activity that consists of at least a *Name, Description, Location, Start- & Endtime*. Multiple activities can be created at once, if passed in an array. However, be aware that creating multiple activities at once is done in sequence. An error in one of the submitted activities will cause its creation to fail and an error response will be returned. However, all other activities may be created, as long as they aren't faulty themselves!

|     |     |
| --- | --- |
| **URL** | `/activities` |
| **Method** | POST |
| **URL Parameters** | *None* |
| **Data Parameters** | Required Properties: *name, description, location, startDate, endDate* |
| | Optional Properties: *category, priority, participants, done* |
| | [View Example Body](https://codebeautify.org/online-json-editor/cbc8dfcc) |
| **Success&#160;Response** | 201 Created - [View Content Example](https://codebeautify.org/online-json-editor/cb9865da) |
| **Error Response** | 400 Bad Request - [View Content](https://codebeautify.org/online-json-editor/cb24e991) (Required Field Missing) |
| | 400 Bad Request - [View Content](https://codebeautify.org/online-json-editor/cb305b9c) (Invalid Date Error) |

```sh
# Example Call - Creating a new activity
curl --request POST --url 'http://example.com:3000/api/activities' \
  --header 'Content-Type: application/json' \
  --header 'content-length: 164' \
  --data '[{"name": "Birthday Party","description": "Remember to bring the cake","location": \
    "Mr. Smiths Home","startDate": "2019-11-15 18:00","endDate": "2019-11-15 23:00"}]'
```


#### Delete Single Activity
---
Delete a single activity based on the specified ID.

|     |     |
| --- | --- |
| **URL** | `/activities/{id}` |
| **Method** | DELETE |
| **URL Parameters** | `id=[string]` *required* - ID of the activity to delete |
| **Data Parameters** | *None* |
| **Success&#160;Response** | 204 No Content - *No Content* |
| **Error Response** | 404 Not Found - *No Content* |

```sh
# Example Call - Delete activity with ID '5cc1ec46a9bf750ea4e1916d'
curl --request DELETE --url 'http://example.com:3000/api/activities/5cc1ec46a9bf750ea4e1916d'
```


#### Update Single Activity
---
Update one ore more properties of a single activity.

|     |     |
| --- | --- |
| **URL** | `/activities/{id}` |
| **Method** | PUT |
| **URL Parameters** | `id=[string]` *required* - ID of the activity to update |
| **Data Parameters** | [View Example Body](https://codebeautify.org/online-json-editor/cb8c69ec) |
| **Success&#160;Response** | 200 OK - [View Content Example](https://codebeautify.org/online-json-editor/cb5fc251) |
| **Error Response** | 400 Bad Request - [View Content](https://codebeautify.org/online-json-editor/cb2e56d7) (Invalid Date Error) |
| | 400 Bad Request - [View Content](https://codebeautify.org/online-json-editor/cbb222c5) (Invalid ID Error) |
| | 404 Not Found - *No Content* |

```sh
# Example Call - Update the 'name' and 'startDate' of activity '5cc36c9368d564376c044ddd'
curl --request PUT --url 'http://example.com:3000/api/activities/5cc36c9368d564376c044ddd' \
  --header 'Content-Type: application/json' \
  --header 'content-length: 42' \
  --data '{"name": "Birthday Party Mr. Smith","startDate": "2019-11-15 18:00"}'
```
