import * as bodyParser from 'body-parser';
import chai from 'chai';
import chaiHttp from 'chai-http';
import config from 'config';
import express, { Request, Response } from 'express';
import { Server } from 'http';
import { sanitizer } from '../src/middleware/sanitizer';
import { fail } from 'assert';

chai.use(chaiHttp);
chai.use(require('chai-things'));
chai.should();

describe('Testing middleware', () => {
  let app: express.Application;
  let server: Server;

  beforeEach((done: MochaDone) => {
    app = express();
    server = app.listen(<number>config.get('app.port') + 10, config.get('app.hostname'), () => {
      done();
    });
  });

  afterEach((done: MochaDone) => {
    server.close(() => {
      done();
    });
  });

  describe('sanitizer', () => {
    const testPayload: { [s: string]: string; } = {
      a: '<h1>Headline</h1>',
      b: '<p>Paragraph</p>',
      c: '<b>Bold</b>',
      d: '<strong>Strong</strong>',
      e: '<script>alert("Script");</script>',
      f: '<html><head>Head</head><body>Body</body></html>',
      g: '<img href="some_image_url" >',
      h: '<a href="some_url">Link</a>',
      i: '<proprietary>Proprietary</proprietary>',
      j: 'Arrow =>',
    };

    const expectedResults: { [s: string]: string; } = {
      a: 'Headline',
      b: 'Paragraph',
      c: 'Bold',
      d: 'Strong',
      e: '',
      f: 'HeadBody',
      g: '',
      h: 'Link',
      i: 'Proprietary',
      j: 'Arrow =&gt;',
    };

    it('should remove all HTML and scripts from the request body', (done: MochaDone) => {
      app.use(bodyParser.text({ type: '*/*' }));
      app.use(sanitizer);
      app.use((request: Request, response: Response) => { response.send({ data: request.body }); });

      chai.request(app).post('/').send(testPayload).end((error, response) => {
        for (const i in testPayload) {
          response.body.data.should.not.contain(testPayload[i]);
          response.body.data.should.contain(`"${i}":"${expectedResults[i]}"`);
        }

        done();
      });
    });

    it('should remove all HTML and scripts from the requests query params', (done: MochaDone) => {
      app.use(bodyParser.text({ type: '*/*' }));
      app.use(sanitizer);
      app.use((req: Request, res: Response) => { res.send({ data: req.query }); });

      const query = [];
      for (const i in testPayload) {
        query.push(`${i}=${testPayload[i]}`);
      }

      chai.request(app).get(`/?${query.join('&')}`).end((error, response) => {
        for (const i in testPayload) {
          response.body.data.should.not.have.property(i, testPayload[i]);
          response.body.data.should.have.property(i, expectedResults[i]);
        }

        done();
      });
    });

    it('its result should still be parsable JSON', (done: MochaDone) => {
      app.use(bodyParser.text({ type: '*/*' }));
      app.use(sanitizer);
      app.use((request: Request, response: Response) => { response.send({ data: request.body }); });

      chai.request(app).post('/').send(testPayload).end((error, response) => {
        try {
          JSON.parse(response.body.data);
          done();
        } catch (e) {
          fail('Failed to parse response');
        }
      });
    });
  });
});
