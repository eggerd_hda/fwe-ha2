import { fail } from 'assert';
import chai from 'chai';
import chaiHttp from 'chai-http';
import csvtojson from 'csvtojson';
import { Response } from 'superagent';
import { app } from '../src/index';
import { activityModel, IActivityModel } from '../src/model/activity';

chai.use(chaiHttp);
chai.use(require('chai-things'));
chai.should();

/**
 * Validates that the response has a certain status and that its body contains an error with a
 * certain name and message
 *
 * @param {Response} response the response of the Chai request
 * @param {number} status the expected status code
 * @param {string} name the expected error name
 * @param {RegEx} message a regular expression that matches the expected error message
 */
const shouldBeError = (response: Response, status: number, name: string, message?: RegExp) => {
  response.should.have.status(status);
  response.body.should.have.key('error');
  response.body.error.should.have.keys(['name', 'message']);
  response.body.error.name.should.equal(name);
  if (message) { response.body.error.message.should.match(message); }
};

/**
 * Validates that the response has a certain status and that its body is empty
 *
 * @param {Response} response the response of the Chai request
 * @param {number} status the expected status code
 */
const shouldHaveEmptyBody = (response: Response, status: number) => {
  response.should.have.status(status);
  response.body.should.deep.equal({});
};

describe('Testing /api/activities', () => {
  // list of all activity properties that should be included in a response
  const responseProperties = ['_id', 'name', 'description', 'location', 'startDate', 'endDate',
    'createdAt', 'updatedAt', 'done', 'priority'];

  // some test data containing a past, present, and future activity (at least for a few decades)
  const basicTestData = [
    {
      name: 'Birth',
      description: 'Hello World!',
      location: 'Groß-Umstadt, Germany',
      startDate: '1992-11-15 13:37',
      endDate: '1992-11-15 14:00',
      category: 'Entertainment',
      done: true,
    },
    {
      name: 'Work',
      description: 'Germans always Wörk Wörk',
      location: 'Germany',
      startDate: '2010',
      endDate: '2060',
      priority: 5,
    },
    {
      name: 'Retirement',
      description: 'But at some point, it has been enough Wörk',
      location: 'Maybe on Mars?',
      startDate: '2060',
      endDate: '2099',
      priority: 10,
      participants: ['Me', 'My future cat'],
    },
  ];

  // will hold the activities existing in the database
  let dbData: IActivityModel[];

  /**
   * Clear database and then insert test data, before running the tests
   */
  const setupDatabase = () => {
    before(function (done: MochaDone) {
      dbData = [];
      this.timeout(5000); // default timeout is (often) too fast for this

      activityModel.deleteMany({}, () => {
        activityModel.create(basicTestData, (error, response) => {
          dbData = response;
          done();
        });
      });
    });
  };

  /**
   * Clear database before running the tests
   */
  const clearDatabase = () => {
    before((done: MochaDone) => {
      dbData = [];
      activityModel.deleteMany({}, () => {
        done();
      });
    });
  };

  /**
   * Getting multiple activities, based on their date
   */
  describe('GET /', () => {
    const currentDate = new Date().toISOString().slice(0, 10); // current date in YYYY-MM-DD format
    setupDatabase(); // clear & insert test data to use for all tests in this suite

    it('should contain all required fields, and the date that has been used', (done: MochaDone) => {
      chai.request(app).get('/api/activities').end((error, response) => {
        const body = response.body;
        response.should.have.status(200);

        // apart from the activities, the dated used should also be included
        body.should.have.keys(['date', 'data']);

        // should contain all the expected fields
        for (const i in responseProperties) {
          body.data.should.all.have.property(responseProperties[i]);
        }

        done();
      });
    });

    it('should by default only get present and future activities', (done: MochaDone) => {
      chai.request(app).get('/api/activities').end((error, response) => {
        const body = response.body;
        response.should.have.status(200);

        // current date should have been used for the query, as no date was specified
        body.date.should.match(new RegExp(currentDate));

        // shouldn't contain the "Birth" activity, as it lies in the past
        body.data.should.not.contain.an.item.with.property('_id', `${dbData[0].id}`);

        // should only contain the "Work" and "Retirement" activity
        body.data.should.contain.an.item.with.property('_id', `${dbData[1].id}`); // present
        body.data.should.contain.an.item.with.property('_id', `${dbData[2].id}`); // future
        body.data.length.should.equal(2);
        done();
      });
    });

    it('should allow to only get past activities, instead of future ones', (done: MochaDone) => {
      chai.request(app).get('/api/activities?past').end((error, response) => {
        const body = response.body;
        response.should.have.status(200);

        // current date should have been used for the query, as no date was specified
        body.date.should.match(new RegExp(currentDate));

        // should only contain the "Birth" activity, as it lies in the past
        body.data.should.contain.an.item.with.property('_id', `${dbData[0].id}`);
        body.data.length.should.equal(1);
        done();
      });
    });

    it('should allow to specify a date to use for the query', (done: MochaDone) => {
      chai.request(app).get('/api/activities?date=2090').end((error, response) => {
        const body = response.body;
        response.should.have.status(200);

        // the date used should be based on the date specified in the query
        body.date.should.contain('2090');

        // should only contain the "Retirement" activity, as it lies in that date
        body.data.should.contain.an.item.with.property('_id', `${dbData[2].id}`);
        body.data.length.should.equal(1);
        done();
      });
    });

    it('should return an error, if an invalid date has been specified', (done: MochaDone) => {
      chai.request(app).get('/api/activities?date=invalid').end((error, response) => {
        shouldBeError(response, 400, 'InvalidDateError');
        done();
      });
    });

    it('should return an empty data array, if no activities exist', (done: MochaDone) => {
      chai.request(app).get('/api/activities?date=2999').end((error, response) => {
        const body = response.body;
        response.should.have.status(200);

        // the response should be an empty data array
        body.should.contain.key('data');
        body.data.should.deep.equal([]);
        done();
      });
    });
  });

  /**
   * Getting a single activity by ID
   */
  describe('GET /:id', () => {
    setupDatabase(); // clear & insert test data to use for all tests in this suite

    it('should only return the requested activity', (done: MochaDone) => {
      chai.request(app).get(`/api/activities/${dbData[0].id}`).end((error, response) => {
        const body = response.body;
        response.should.have.status(200);

        // should only contain the requested activity
        body.data.should.be.an('object');
        body.data.should.contain.property('_id', `${dbData[0].id}`);

        // should contain all the expected fields
        for (const i in responseProperties) {
          body.data.should.have.property(responseProperties[i]);
        }

        done();
      });
    });

    it('should return an error, if an invalid ID has been specified', (done: MochaDone) => {
      chai.request(app).get('/api/activities/invalidId').end((error, response) => {
        shouldBeError(response, 400, 'InvalidIdError');
        done();
      });
    });

    it('should return status 404, if the requested activity does not exit', (done: MochaDone) => {
      chai.request(app).get('/api/activities/5cc1ec46a9bf750ea4e19169').end((error, response) => {
        shouldHaveEmptyBody(response, 404);
        done();
      });
    });
  });

  /**
   * Exporting all activities as a CSV file
   */
  describe('GET /csv', () => {
    setupDatabase(); // clear & insert test data to use for all tests in this suite

    it('should return all activities as CSV file', (done: MochaDone) => {
      chai.request(app).get('/api/activities/csv').buffer().end((error, response) => {
        const header = response.header;
        response.should.have.status(200);

        header.should.have.a.property('content-type', 'text/csv; charset=utf-8');
        header.should.have.a.property('content-disposition', 'attachment; filename=activities.csv');

        csvtojson().fromString(response.text).then((csv) => {
          response.body = csv; // using the response.body, because otherwise "an" doesn't work
          const body = response.body;

          // should contain all three activities
          body.should.contain.an.item.with.property('_id', `${dbData[0].id}`);
          body.should.contain.an.item.with.property('_id', `${dbData[1].id}`);
          body.should.contain.an.item.with.property('_id', `${dbData[2].id}`);
          body.length.should.equal(3);

          // should contain all the expected fields
          for (const i in responseProperties) {
            body.should.all.have.property(responseProperties[i]);
          }

          done();
        });
      });
    });

    // needs its own suite, so that the database can be cleared before running the test
    describe('if there are no activities to export', () => {
      clearDatabase();

      it('should return status 204', (done: MochaDone) => {
        chai.request(app).get('/api/activities/csv').end((error, response) => {
          shouldHaveEmptyBody(response, 204);
          done();
        });
      });
    });
  });

  /**
   * Creating a new activity
   */
  describe('POST /', () => {
    clearDatabase();

    it('should return the created activity and ignore all unknown fields', (done: MochaDone) => {
      const activity = {
        name: 'the name',
        description: 'the description',
        location: 'the location',
        startDate: '2019',
        endDate: '2019-12-31',
        unknown1: 'This field does not exist',
        unknown2: 'Neither does this',
      };

      chai.request(app).post('/api/activities').send(activity).end((error, response) => {
        const body = response.body.data[0];

        // should have status for "Created"
        response.should.have.status(201);

        // should have the values that have been specified
        body.should.be.an('object');
        body.name.should.equal(activity.name);
        body.description.should.equal(activity.description);
        body.location.should.equal(activity.location);
        body.startDate.should.contain('2019-01-01');
        body.endDate.should.contain('2019-12-31');

        // should have ignored the unknown fields
        body.should.not.have.property('unknown1');
        body.should.not.have.property('unknown2');

        // should have been added to the database
        activityModel.findById(body._id, (error, found) => {
          if (!found) { fail('Expected activity to exist in the database'); }
          done();
        });
      });
    });

    it('should remove all HTML and scripts from the input', (done: MochaDone) => {
      chai.request(app).post('/api/activities').send({
        name: '<h1>the name</h1>',
        description: '<p>the description</p>',
        location: 'the location <script>alert("FBI, open up!")</script>',
        category: '<h3>the category</h3>',
        participants: ['<li>Me</li>', '<li>He</li>'],
        startDate: '2018',
        endDate: '2019',
      }).end((error, response) => {
        const body = response.body.data[0];

        // should have status for "Created"
        response.should.have.status(201);

        // HTML and scripts should have been removed
        body.should.be.an('object');
        body.name.should.equal('the name');
        body.description.should.equal('the description');
        body.location.should.equal('the location');
        body.category.should.equal('the category');
        body.participants.should.have.members(['Me', 'He']);

        // should have been added to the database
        activityModel.findById(body._id, (error, found) => {
          if (!found) { fail('Expected activity to exist in the database'); } else {
            // HTML and scripts should have been removed before adding it to the database
            found.name.should.equal('the name');
            found.description.should.equal('the description');
            found.location.should.equal('the location');
            found.category.should.equal('the category');
            found.participants.should.have.members(['Me', 'He']);
          }
          done();
        });
      });
    });

    it('should return an error, if endDate is earlier than startDate', (done: MochaDone) => {
      chai.request(app).post('/api/activities').send([
        {
          name: 'the name',
          description: 'the description',
          location: 'the location',
          startDate: '2017',
          endDate: '2018',
        },
        {
          name: 'the name',
          description: 'the description',
          location: 'the location',
          startDate: '2019',
          endDate: '2018',
        },
      ]).end((error, response) => {
        shouldBeError(response, 400, 'ValidationError', /.*endDate.*startDate.*/i);
        done();
      });
    });

    it('should return an error, if a required field is missing', (done: MochaDone) => {
      chai.request(app).post('/api/activities').send({
        name: 'the name',
        description: 'the description',
        startDate: '2018',
        endDate: '2019',
      }).end((error, response) => {
        shouldBeError(response, 400, 'ValidationError', /.*location.*is required.*/i);
        done();
      });
    });

    it('should return an error, if an invalid value is used', (done: MochaDone) => {
      chai.request(app).post('/api/activities').send({
        name: ' ',
        description: 'the description',
        startDate: 'this is not a date',
        endDate: '2019',
      }).end((error, response) => {
        shouldBeError(response, 400, 'ValidationError', /.*cast to date failed.*name.*required.*/i);
        done();
      });
    });
  });

  /**
   * Updating an existing activity by ID
   */
  describe('PUT /:id', () => {
    setupDatabase(); // clear & insert test data to use for all tests in this suite

    it('should return the updated activity and ignore all unknown fields', (done: MochaDone) => {
      chai.request(app).put(`/api/activities/${dbData[1].id}`).send({
        name: 'the new name',
        category: 'adding a category',
        unknown1: 'This field does not exist',
      }).end((error, response) => {
        const body = response.body.data;
        response.should.have.status(200);

        // should only contain the updated activity
        body.should.be.an('object');
        body.should.contain.property('_id', `${dbData[1].id}`);

        // values should have been updated
        body.name.should.equal('the new name');
        body.category.should.equal('adding a category');

        // should have ignored the unknown field
        body.should.not.have.property('unknown1');

        // database should have been updated
        activityModel.findById(body._id, (error, found) => {
          if (!found) { fail('Expected activity to exist in the database'); } else {
            found.name.should.equal('the new name');
            found.category.should.equal('adding a category');
            done();
          }
        });
      });
    });

    it('should remove all HTML and scripts from the input', (done: MochaDone) => {
      chai.request(app).put(`/api/activities/${dbData[0].id}`).send({
        name: '<h1>the new name</h1>',
        description: '<p>the new description</p>',
        location: 'the new location <script>alert("FBI, open up!")</script>',
      }).end((error, response) => {
        const body = response.body.data;
        response.should.have.status(200);

        // should only contain the updated activity
        body.should.be.an('object');
        body.should.contain.property('_id', `${dbData[0].id}`);

        // values should have been updated
        body.name.should.equal('the new name');
        body.description.should.equal('the new description');
        body.location.should.equal('the new location');

        // database should have been updated
        activityModel.findById(body._id, (error, found) => {
          if (!found) { fail('Expected activity to exist in the database'); } else {
            // HTML and scripts should have been removed before updating the database
            found.name.should.equal('the new name');
            found.description.should.equal('the new description');
            found.location.should.equal('the new location');
            done();
          }
        });
      });
    });

    it('should return an error, if an invalid value is used', (done: MochaDone) => {
      chai.request(app).put(`/api/activities/${dbData[0].id}`).send({
        startDate: 'this is not a date',
      }).end((error, response) => {
        shouldBeError(response, 400, 'CastError', /.*cast to date failed.*/i);

        chai.request(app).put(`/api/activities/${dbData[0].id}`).send({
          name: '',
        }).end((error, response) => {
          shouldBeError(response, 400, 'ValidationError', /.*name.*is required.*/i);
          done();
        });
      });
    });

    it('should return an error, if endDate is earlier than startDate', (done: MochaDone) => {
      chai.request(app).put(`/api/activities/${dbData[0].id}`).send({
        name: 'the name',
        description: 'the description',
        location: 'the location',
        startDate: '2019',
        endDate: '2018',
      }).end((error, response) => {
        shouldBeError(response, 400, 'ValidationError', /.*endDate.*startDate.*/i);
        done();
      });
    });

    it('should return an error, if an invalid ID has been specified', (done: MochaDone) => {
      chai.request(app).put('/api/activities/invalidId').end((error, response) => {
        shouldBeError(response, 400, 'InvalidIdError');
        done();
      });
    });

    it('should return status 404, if the requested activity does not exit', (done: MochaDone) => {
      chai.request(app).put('/api/activities/5cc1ec46a9bf750ea4e19169').end((error, response) => {
        shouldHaveEmptyBody(response, 404);
        done();
      });
    });
  });

  /**
   * Deleting an activity by ID
   */
  describe('DELETE /:id', () => {
    setupDatabase(); // clear & insert test data to use for all tests in this suite

    it('should return status 204, if the activity has been deleted', (done: MochaDone) => {
      chai.request(app).delete(`/api/activities/${dbData[0].id}`).end((error, response) => {
        shouldHaveEmptyBody(response, 204);

        // should no longer exist in the database
        activityModel.findById(dbData[0].id, (error, found) => {
          if (found) { fail('Expected activity to be missing from the database'); }
          done();
        });
      });
    });

    it('should return status 404, if the requested activity does not exit', (done: MochaDone) => {
      chai.request(app).delete('/api/activities/5cc1ec46a9bf750ea4e19169').end((err, response) => {
        shouldHaveEmptyBody(response, 404);
        done();
      });
    });

    it('should return an error, if an invalid ID has been specified', (done: MochaDone) => {
      chai.request(app).delete('/api/activities/invalidId').end((error, response) => {
        shouldBeError(response, 400, 'InvalidIdError');
        done();
      });
    });
  });
});
