FROM mhart/alpine-node

COPY backend/ /backend/
COPY frontend/ /frontend/
COPY example-data.zip /data/db/

RUN echo 'http://dl-3.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories
RUN apk upgrade --update
RUN apk add --no-cache mongodb
RUN (cd /data/db/ && unzip example-data.zip && rm example-data.zip)
RUN (cd /backend && npm ci --prod)
RUN npm install -g http-server

CMD mongod & \
    (cd /backend && node dist/index.js) & \
    (cd /frontend/dist && http-server --gzip --proxy http://localhost:8080?)

EXPOSE 8080 3000 27017
